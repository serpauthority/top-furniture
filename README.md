Choosing the Right Colours When Redecorating Your Home
======================================================

The colours we love are very personal to us, and while colours have been shown to have an emotive effect which is fairly consistent they can be used to great effect in the home when they are chosen well. One of the most important tools you can use to help you choose the perfect colours for your interior spaces is the colour wheel. This is simply a representation of the colour families, and their position on the wheel relative to other colours determines whether those colours will complement or contrast.

Colours which are placed opposite one another on the colour wheel are ones which will contrast, while adjacent colours will harmonise and complement each other well to create a more relaxing look. The different ways that colours interact can be described as colour theory, and this is something that can really help you to make your choice. The most commonly used colour wheels have 12 hues which can be described as cool or warm, and there are also primary, secondary and tertiary colours which radiate out from the centre.

Before we go into colour theory in more detail, it is important to make a few distinctions for the terminology used.

-   Hue: a colour or shade of a colour such as blue, green, pink or purple
-   Primary colours: the first colours to appear on the colour wheel are red, yellow and blue
-   Secondary colours: these colours are made by mixing the primary colours and they are orange, purple and green
-   Tertiary colours: you get a tertiary colour when you mix a primary and secondary colour together, there are many tertiary colours including lime green, magenta, cyan, and indigo
-   Tint: you can alter the tint of a colour by adding more black or white to the base hue
-   Tone: the tone of a colour is used to describe its depth and brightness. You can give a colour a more tonal look by adding grey to the base hue
-   Shade: the shade of a colour describes how light or dark the colour is, such as navy blue and sky blue are both shades of blue which look very different in terms of lightness and darkness

It is possible to apply colour to any aspect of the home such as walls, floor coverings, rugs, upholstery, <span style="text-decoration: underline;">***[oak shelving units](https://www.topfurniture.co.uk/oak-storage-furniture/oak-bookcases.html)***</span> or decorative items. There are many ways to apply colour to the various rooms of your home, and we will cover some of the most popular ones in this article.

Monochrome
----------

While most people think of monochrome as relating to black and white, it actually refers to colour looks which make use of one main colour. This can be varied by altering the tones and shades of the colour used. For example, you can create a monochrome look by combining various shades of blue. The more shades you include the more textural and interesting the overall look will be, while keeping with a monochromatic theme.

Harmonise
---------

When you choose colours which appear next to each other on the colour wheel you can create a harmonious look which is relaxing and easy on the eye. Pick out two or more colours which are complementary to one another and use these in your interior spaces. Examples of colours which harmonise include orange and yellow, yellow and green, blue and purple, and red and orange.

High contrast
-------------

Contrasting colours sit opposite each other on the colour wheel, and create a bold and energising look. It is important to choose colours which are still complimentary as this will prevent the space from feeling jarring and overbearing. Good contrasting options to try are purple and yellow, pink and green, orange and blue, yellow and blue, yellow and pink, green and orange, and blue and orange.

Go with your instincts
----------------------

When it comes to choosing colours it is usually best to go with your gut reaction. If you feel drawn to particular colour combinations then the chances are there will be a way for you to make it work. Most interior designers are not afraid to make bold and unusual colour choices, and these are largely chosen instinctively as this is the best way to make sure your colour schemes have the desired impact in your home.

### Resources:

-   [Living Room Sideboards - Beijing University](https://gitlab.educg.net/serpauthority/top-furniture)
-   [Dining Room Sideboards - Bootcamp](https://vanderbilt.bootcampcontent.com/serpauthority/top-furniture)
-   [Oak Sideboards - Code Dream](https://git.codesdream.com/serpauthority/top-furniture/-/blob/master/README.md)
-   [Oak Blanket Boxes - Gitee](https://gitee.com/serpauthority/top-furniture)
-   [Oak Wardrobes - GNOME](https://gitlab.gnome.org/serpauthority/top-furniture)
-   [Oak Dining Tables - Hope](https://gitlab.eduxiji.net/serpauthority/top-furniture)
-   [10 Seater Dining Table - Montera34](https://code.montera34.com/serpauthority/top-furniture)
-   [Large Dining Tables - Renku](https://renkulab.io/gitlab/serp-authority/top-furniture)
